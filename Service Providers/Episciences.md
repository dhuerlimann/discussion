# [Episciences](https://www.episciences.org/page/general-informations/?lang=en)

* For overlay journals, open access, free to read, free to publish.
* Each journal has its own personalized web site.
* Articles are hosted on separate archive servers (HAL, arXiv, CWI).
* Platform facilitates the editorial process: submission of manuscripts, peer reviewing management, publication.
* Accepted papers are returned in their final version to the original archive, and links on the overlay journal web site are automatically created.
* The Epimath branch of Episciences has a customizable default LaTeX style.
* If desired, copy editing of the final version of papers can be individually supported by each overlay journal, through ad hoc resources.
* Developed and managed by the CCSD (Centre pour la Communication Scientifique Directe).

### [Example journals](https://www.episciences.org/page/journals) publishing with Episciences
* [Epijournal de Géométrie Algébrique](https://epiga.episciences.org/)
* [Hardy-Ramanujan Journal ](hrj.episciences.org)
* [Logical Methods in Computer Science](https://lmcs.episciences.org/)