# [Ubiquity Press](http://www.ubiquitypress.com/)

* Hosted and full-service journals
* OA journals with CC BY, but with APC (£400-500)
* Editorial system is modern, based on Open Journal Systems, but nicer
* Experience of transferring/flipping journals from all major publishers
* Supports alternative research outputs such as data and software (as 'metajournals' or as data/software papers within standard journals)

The company is a spinoff from University College London, and now has offices in London and Berkeley.

### Services
Two main levels of service:
* *Hosted journals*: Ubiquity Press simply hosts the journals on our platform and ensures that they are fully functional and available. The client institution is responsible for editorial support and ensuring that functions such as peer review, copyediting and typesetting are carried out. These journals have a flat annual fee of £750 or ca. €850.
* *Full-service journals*: Here we provide a full stack of publishing services, including dedicated editorial support, anti-plagiarism checking, copyediting, typesetting, indexing and archiving and content promotion, etc.. These journals have no annual charge, but do incur a fee per article published of £400/ca.€450  (HSS) or £500/ca.€565 (STEM). These fees are generally paid by either the Society, or by the author's funder or institution [**Note**: Compliance with Fair OA principle necessarily entails that the APC are paid journal-side rather than author-side.]
* *Optional services*: Both hosted and full service journals can elect to use additional services. A hosted journal for example may like to add typesetting, while full service journal may opt for more advanced copyediting or an alternative editorial management system. These services are charged on either an annual or per-article basis, as appropriate.

Intermediate prices are possible, the services can be personalised feature by feature. 

### [Example journals](https://www.ubiquitypress.com/site/journals/) publishing with Ubiquity Press
* [Glossa](http://www.glossa-journal.org)
* [Laboratory Phonology](https://www.journal-labphon.org/)
* [Journal of Open Research Software](https://openresearchsoftware.metajnl.com/) (Metajournal)

### More links
* [An editor training video](https://vimeo.com/144631620) (password: JMSTraining)