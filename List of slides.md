* [Presentations about OA by AOASG](https://aoasg.org.au/presentations-about-oa/) 
* [Benoît R. Kloeckner 2017 Lumigny](http://perso-math.univ-mlv.fr/users/kloeckner.benoit/files/ANF-RNBM-2017.pdf) (In French)
* [J-C Guedon Unleashing knowledge with Open Access](http://doi.org/10.5281/zenodo.1250185)