# Costs and ways to obtain funding

*to be written, help if you can*

### Costs
* [ISSN](http://www.issn.org/services/requesting-an-issn/) is free
* Domain - few $/year
* Hosting - 0 to maybe $60/year 
* DOI numbers/CrossRef membership - €75/year via [OASPA](oaspa.org) (otherwise $275/year)
* [CLOCKSS](https://www.clockss.org/clockss/Contribute_to_CLOCKSS) - $232/year, $0.25/article (first 100 free)
* Journal management system & publishing service - depends (see [here](https://gitlab.com/publishing-reform/discussion/tree/master/Service%20Providers))

So overall, the fixed costs amount to around  $350/year. On top of that can come extra costs per submission or accepted papers, depending on the journal management system/publishing service provider and whether accepted papers require additional copy editing, typesetting and proofreading. However, keep in mind the Fair OA Principles 4 and 5:
> 4. Submission and publication is not conditional in any way on the payment of a fee from the author or its employing institution, or on membership of an institution or society.
> 5. Any fees paid on behalf of the journal to publishers are low, transparent, and in proportion to the work carried out.

That means that any APC (article processing charges) or charges for submissions should not be enforced on the authors, but have to be covered by the journal. It is therefore common practice for Fair OA journals to provide their authors with stylesheets and rules for typesetting, so that authors can typeset their papers themselves. 

### Institutional support
University libraries or other institutions may be able to provide server space, funds, and clerical support. ...

### Partnerships

### Funds

### Donations and voluntary APC

